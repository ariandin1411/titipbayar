const HttpException = require('../utils/HttpException.utils');
const AuthModel = require('../models/auth.model');
// const RoleUserModel = require('../models/roleUser.model');
// const { roleIdToName } = require('../utils/userRolesToName.utils');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
dotenv.config();
const logger = require('../middleware/logger');

const auth = (...roles) => {
  return async function (req, res, next) {
    try {

      logger.info('[auth.middleware.validateIP] start');
      const request_ip = req.ip 
            || req.connection.remoteAddress 
            || req.socket.remoteAddress 
            || req.connection.socket.remoteAddress;

      const whitelist_ip = process.env.WHITELIST_IP.split(",");

      logger.info('[auth.middleware.validateIP] request_ip: ' + request_ip);
      logger.info('[auth.middleware.validateIP] list_ip: ' + JSON.stringify(request_ip));

      if (!whitelist_ip.includes(request_ip)) {
        throw new HttpException(401, 'Access denied. network not valid!');
      }
      logger.info('[auth.middleware.validateIP] end');

      const authHeader = req.headers.authorization;
      const bearer = 'Bearer ';

      if (!authHeader || !authHeader.startsWith(bearer)) {
        throw new HttpException(401, 'Access denied. No credentials sent!');
      }

      const token = authHeader.replace(bearer, '');
      const secretKey = process.env.SECRET_JWT || '';

      // Verify Token
      const decoded = jwt.verify(token, secretKey);
      const user = await AuthModel.findOne({ id: decoded.user_id });

      if (!user) {
        throw new HttpException(401, 'Authentication failed!');
      }

      // Check keydesk
      // const userKeyDesk = await UserModel.findOne({ desk_login: req.headers.keydesk });
      // if (!userKeyDesk) {
      //   throw new HttpException(401, 'Authentication desk_login failed!');
      // }

      // Check keydesk
      // const userRole = await RoleUserModel.findOne({ user_id: user.id });
      // if (!userRole) {
      //   throw new HttpException(401, 'Authentication desk_login failed!');
      // }
      // let roleName = roleIdToName(userRole.role_id);
      // if (!roleName) {
      //   throw new HttpException(401, 'Authentication desk_login failed!');
      // }

      const ownerAuthorized = req.params.id === user.id;
      // if (!ownerAuthorized && roles.length && !roles.includes(roleName)) {
      //   throw new HttpException(401, 'Unauthorized');
      // }

      // if the user has permissions
      req.currentUser = user;
      next();

    } catch (e) {
      e.status = 401;
      next(e);
    }
  }
}

module.exports = auth;
