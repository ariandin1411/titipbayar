const opts = {
  errorEventName:'error',
  logDirectory:'./log', // NOTE: folder must exist and be writable...
  fileNamePattern:'auth-log-<DATE>.log',
  dateFormat:'YYYY-MM-DD'
};
const log = require('simple-node-logger').createRollingFileLogger( opts );

module.exports = log;
