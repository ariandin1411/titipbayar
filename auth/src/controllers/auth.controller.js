const AuthModel = require('../models/auth.model');
const HttpException = require('../utils/HttpException.utils');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const logger = require('../middleware/logger');
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              User Controller
 ******************************************************************************/
class AuthController {
  getCurrentUser = async (req, res) => {
    logger.info('[auth.controller getCurrentUser] start.');
    logger.info('[auth.controller getCurrentUser] request params : ' + JSON.stringify(req.body));
    const { password, phone2, active, login_date, created_at, created_by, created_date,
      updated_at, updated_by, updated_date, ...userWithoutPassword } = req.currentUser;

    res.send(userWithoutPassword);
  };

  createUser = async (req, res) => {
    logger.info('[user.controller createUser] start.');
    logger.info('[user.controller createUser] request params : ' + JSON.stringify(req.body));
    this.checkValidation(req);
    await this.hashPassword(req);


    // req.body.created_by = req.currentUser.id;
    req.body.active = 1;
    req.body.created_by = req.body.full_name;
    req.body.created_date = moment().format('YYYY-MM-DD H:mm:ss');
    //req.body.updated_date = moment().format('YYYY-MM-DD H:mm:ss');

    const {confirm_password, ...reqBody} = req.body;

    const result = await AuthModel.create(reqBody);

    if (!result) {
      throw new HttpException(500, 'Something went wrong');
    }
    

    let resultResponse = {
      resultCd: '00',
      resultMsg: 'Success',
      data: {
        insertId: result.insertId
      }
    };

    res.status(201).send(resultResponse);
  };

  authLogin = async (req, res) => {
    logger.info('[auth.controller authLogin] start.');
    logger.info('[auth.controller authLogin] request params : ' + JSON.stringify(req.body));

    this.checkValidation(req);

    const { email, password: pass } = req.body;
    const user = await AuthModel.findOne({ email });

    if (!user) {
      logger.info('[auth.controller authLogin] cant find user');
      throw new HttpException(401, 'Cannot find email!');
    }

    const isMatch = await bcrypt.compare(pass, user.password);

    if (!isMatch) {
      logger.info('[auth.controller authLogin] password not matched');
      throw new HttpException(401, 'Incorrect password!');
    }

    // update
    const obj = {
      login_date: moment().format('YYYY-MM-DD H:mm:ss')
    };
    const result = await AuthModel.updateLoginDate(obj, user.id);

    if (!result) {
      throw new HttpException(500, 'Something went wrong');
    }

    // user matched!
    const secretKey = process.env.SECRET_JWT || "";
    const token = jwt.sign({ user_id: user.id.toString() }, secretKey, {
      expiresIn: '24h'
    });

    const { password, phone2, active, login_date, created_at, created_by, created_date,
      updated_at, updated_by, updated_date, 
      ...userWithoutPassword } = user;


    res.send({ ...userWithoutPassword, token });
  };

  checkValidation = (req) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      throw new HttpException(400, 'Validation faild', errors);
    }
  }

  // hash password if it exists
  hashPassword = async (req) => {
    if (req.body.password) {
      req.body.password = await bcrypt.hash(req.body.password, 8);
    }
  }
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new AuthController;
