const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors');
const HttpException = require('./utils/HttpException.utils');
const errorMiddleware = require('./middleware/error.middleware');
const AuthRouter = require('./routes/auth.route');


const app = express();
dotenv.config();

app.use(express.json());
app.use(cors());
app.use(`/api/v1`, AuthRouter);
app.options('*', cors());

const port = Number(process.env.PORT || 3331);

// 404 error
app.all('*', (req, res, next) => {
  const err = new HttpException(404, 'Endpoint Not Found');
  next(err);
});

app.use(errorMiddleware);

app.listen(port, () => {
    console.log(`Auth Mikro Service`);
    console.log(`🚀 Server running on port ${port}!`);
});

module.exports = app;
