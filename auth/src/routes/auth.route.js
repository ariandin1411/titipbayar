const express = require('express');
const router = express.Router();
const AuthController = require('../controllers/auth.controller');
const auth = require('../middleware/auth.middleware');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { validateLogin, createUserSchema } = require('../middleware/authValidator.middleware');


router.post('/auth/login', validateLogin, awaitHandlerFactory(AuthController.authLogin));
router.post('/auth/create-user/', createUserSchema, awaitHandlerFactory(AuthController.createUser)); // localhost:3000/api/v1/users
router.get('/auth/get-current-user/', auth(), awaitHandlerFactory(AuthController.getCurrentUser));

module.exports = router;
