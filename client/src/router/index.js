import Vue from 'vue'
import VueRouter from 'vue-router'
import { initialize } from '../store/helpers/general'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/Home'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/pages/login/Login')
  }
]

const router = new VueRouter({
  routes
});

initialize(store, router)

export default router
