import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { getLocalUser, getToken } from './helpers/auth';

Vue.use(Vuex)

const user = getLocalUser();
const token = getToken();

export default new Vuex.Store({
  state: {
    status: '',
    currentUser: user,
    currentToken: token,
    loading: false,
    auth_error: null,
    isLoggedIn: !!user,
    header: null,
    prodSel: [],
  },
  mutations: {
    login(state) {
      state.loading = true;
      state.auth_error = null;
    },
    loginSuccess(state, payload) {
      state.auth_error = null;
      state.isLoggedIn = true;
      state.loading = false;
      state.currentUser = Object.assign({}, payload, {
        token: payload.token,
      });
      state.currentToken = payload.token;
      localStorage.setItem('userTitipBayar', JSON.stringify(state.currentUser));
      localStorage.setItem('tokenTitipBayar', payload.token);
    },
    loginFailed(state, payload) {
      state.loading = false
      if(payload.resp){
        state.auth_error = payload.resp;
      }else{
        state.auth_error = payload.error;
      }
    },
    logout(state) {
      localStorage.removeItem('userTitipBayar');
      localStorage.removeItem('tokenTitipBayar');
      state.isLoggedIn = false;
      state.currentUser = null;
    },
    prodSel (state, payload) {
      state.prodSel = payload;
    },
    currentToken (state) {
      state.currentToken = token;
    },
    header (state) {
      let config = {
        headers: {
          Authorization: 'Bearer ' + token
        }
      }
      state.header = config;
    }
  },
  actions: {
    login(context) {
      context.commit('login');
    },
    logout({ commit }) {
      return new Promise((resolve) => {
        commit('logout')
        localStorage.removeItem('userTwitsPpob')
        localStorage.removeItem('userTwitsPpob')
        delete axios.defaults.headers.common['Authorization']
        resolve()
        // console.log(reject)
      })
    }
  },
  modules: {},
  getters: {
    isLoading(state) {
      return state.loading;
    },
    isLoggedIn(state) {
      return state.isLoggedIn;
    },
    currentUser(state) {
      return state.currentUser;
    },
    authError(state) {
      return state.auth_error;
    },
    currentToken(state) {
      return state.currentToken;
    },
    header(state) {
      return state.header
    },
    prodSel (state) {
      return state.prodSel;
    },
  }
})
