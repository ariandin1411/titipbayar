import { setAuthorization } from "./general"
import Axios from 'axios'

const baseUrl = process.env.VUE_APP_AUTH_URL;


export function login(credentials){
  return new Promise((res, rej) => {
    Axios.post(baseUrl + 'login', credentials)
      .then((response) => {
        setAuthorization(response.data.token)
        res(response.data)
      })
      .catch((error) => {
        let errorMsg = null;
        if (error.response) {
          errorMsg = error.response.data;
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
        } else if (error.request) {
          errorMsg = error.request;
          console.log(error.request);
        } else {
          errorMsg = error.message;
          console.log('Error', error.message);
        }
        rej(errorMsg);
      })
  })
}

export function getLocalUser(){
  let userStr = {
    role_id: null
  }
  userStr = localStorage.getItem('userTitipBayar')
  if(!userStr || userStr === null){
    return null
  }
  return JSON.parse(userStr)
}

export function getToken(){
  const token = localStorage.getItem('tokenTitipBayar')
  if(!token || token === null){
    return null;
  }
  return token;
}
