import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import Axios from 'axios';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import moment from 'moment';
require('./store/helpers/formater');


Vue.config.productionTip = false
Vue.prototype.$http = Axios;
Vue.prototype.$moment = moment;
Vue.prototype.$authUrl = process.env.VUE_APP_AUTH_URL;
Vue.prototype.action = new Vue();

const token = localStorage.getItem('access_token');

if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] = 'Bearer ' + token;
  Vue.prototype.$http.defaults.headers.common["Content-Type"] = 'application/json'
  Vue.prototype.$http.defaults.timeout = 15000
  Vue.prototype.$http.defaults.headers.common["Accept"] = 'application/json'
}

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
