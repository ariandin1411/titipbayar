const UserModel = require('../models/user.model');
const HttpException = require('../utils/HttpException.utils');
const { validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const logger = require('../middleware/logger');
const dotenv = require('dotenv');
dotenv.config();

/******************************************************************************
 *                              User Controller
 ******************************************************************************/
class UserController {
  getAllUsers = async (req, res) => {
    logger.info('[auth.controller getCurrentUser] start.');
    logger.info('[auth.controller getCurrentUser] request params : ' + JSON.stringify(req.query));

    

    req.query.active = 1;
   

    let userList = await UserModel.find(req.query);
    if (!userList.length) {
      throw new HttpException(404, 'Users not found');
    }

    userList = userList.map(user => {
      const { password, phone2, active, login_date, created_at, created_by, created_date,
        updated_at, updated_by, updated_date, ...userWithoutPassword } = user;

      return userWithoutPassword;
    });

    res.send(userList);
  };

  getUsersById = async (req, res) => {
    logger.info('[auth.controller getUsersById] start.');
    logger.info('[auth.controller getUsersById] request params : ' + JSON.stringify(req.params));
    
    let params = {  
      active : 1,
      id : parseInt(req.params.id)
    }
   

    let usersObj = await UserModel.findOne(params);
    if (!usersObj) {
      throw new HttpException(404, 'Users not found');
    }

    const { password,email_verification, phone2, active, login_date, created_at, created_by, created_date,
      updated_at, updated_by, updated_date, ...user } = usersObj;

    let resultResponse = {
      resultCd: '00',
      resultMsg: 'Success',
      data: user
    };

    res.status(201).send(resultResponse);

  };

  
}



/******************************************************************************
 *                               Export
 ******************************************************************************/
module.exports = new UserController;
