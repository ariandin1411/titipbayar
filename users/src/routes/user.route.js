const express = require('express');
const router = express.Router();
const UserController = require('../controllers/user.controller');
const auth = require('../middleware/auth.middleware');
const awaitHandlerFactory = require('../middleware/awaitHandlerFactory.middleware');

const { validateLogin, createUserSchema } = require('../middleware/authValidator.middleware');


router.get('/users', auth(), awaitHandlerFactory(UserController.getAllUsers));
router.get('/users/id/:id', auth(), awaitHandlerFactory(UserController.getUsersById));

module.exports = router;
