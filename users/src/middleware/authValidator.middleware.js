const { check } = require('express-validator');

exports.createUserSchema = [
  check('full_name')
    .exists()
    .withMessage('full_name is required')
    .isLength({ min: 3 })
    .withMessage('full_name Must be at least 3 chars long'),
  check('email')
    .exists()
    .withMessage('Email is required')
    .isEmail()
    .withMessage('Email Must be a valid email')
    .normalizeEmail(),
  check('identity_id')
    .exists()
    .withMessage('identity_id is required'),
  check('password')
    .exists()
    .withMessage('Password is required')
    .notEmpty()
    .isLength({ min: 6 })
    .withMessage('Password must contain at least 6 characters')
    .isLength({ max: 10 })
    .withMessage('Password can contain max 10 characters'),
  check('confirm_password')
    .exists()
    .custom((value, { req }) => value === req.body.password)
    .withMessage('confirm_password field must have the same value as the password field'),
  check('phone1')
    .exists()
    .withMessage('phone1 is required'),
  check('address')
    .exists()
    .withMessage('address is required'),

  check('kecamatan')
    .exists()
    .withMessage('kecamatan is required'),
  check('kota')
    .exists()
    .withMessage('kota is required'),
  check('provinsi')
    .exists()
    .withMessage('provinsi is required'),
];

exports.validateLogin = [
  check('email')
    .exists()
    .withMessage('Email is required')
    .isEmail()
    .withMessage('Must be a valid email')
    .normalizeEmail(),
  check('password')
    .exists()
    .withMessage('Password is required')
    .notEmpty()
    .withMessage('Password must be filled')
];
