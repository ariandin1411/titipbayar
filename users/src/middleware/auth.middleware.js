const HttpException = require('../utils/HttpException.utils');
const AuthModel = require('../models/user.model');
// const RoleUserModel = require('../models/roleUser.model');
// const { roleIdToName } = require('../utils/userRolesToName.utils');
const dotenv = require('dotenv');
dotenv.config();
const logger = require('../middleware/logger');
const axios = require('axios');

const auth = (...roles) => {
  return async function (req, res, next) {
    try {

      logger.info('[auth.middleware.validateIP] start');
      const request_ip = req.ip 
            || req.connection.remoteAddress 
            || req.socket.remoteAddress 
            || req.connection.socket.remoteAddress;

      const whitelist_ip = process.env.WHITELIST_IP.split(",");

      logger.info('[auth.middleware.validateIP] request_ip: ' + request_ip);
      logger.info('[auth.middleware.validateIP] list_ip: ' + JSON.stringify(request_ip));

      if (!whitelist_ip.includes(request_ip)) {
        throw new HttpException(401, 'Access denied. network not valid!');
      }
      logger.info('[auth.middleware.validateIP] end');

      const authHeader = req.headers.authorization;
      const bearer = 'Bearer ';

      if (!authHeader || !authHeader.startsWith(bearer)) {
        logger.info('[auth.middleware.validateIP] bearer not send');
        throw new HttpException(401, 'Access denied. No credentials sent!');
      }

      try {
        logger.info('[auth.middleware.validateIP] start check header data');

        let header = {
          headers: {
            Authorization: authHeader,
          }
        }

        let responseAuth = await axios.get(process.env.AUTH_URL + 'get-current-user', header)
        if (responseAuth.data) {
          req.currentUser = responseAuth.data;
          next();
        }
      }catch(err){
        logger.info('[auth.middleware.validateIP] error request from auth  : ' + JSON.stringify(err.response.data.message));
        throw new HttpException(401, err.response.data.message);
      }

    } catch (e) {
      e.status = 401;
      next(e);
    }
  }
}

module.exports = auth;
