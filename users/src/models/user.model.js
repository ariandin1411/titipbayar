const query = require('../db/db-connection');
const {multipleColumnSet, multipleColumnSetWhere} = require('../utils/common.utils');
const moment = require('moment');
const logger = require('../middleware/logger');
const HttpException = require('../utils/HttpException.utils');

class UserModel {
  tableName = 'users';
  id;
  full_name;
  email;
  email_verification;
  password;
  identity_id;
  phone1;
  phone2;
  address;
  kecamatan;
  kota
  provinsi;
  kodepos;
  active;
  login_date;
  created_by;
  created_date;
  updated_by;
  updated_date;


  find = async (params = {}) => {
    logger.info('[auth.model find] start.');
    let sql = this.selectQuery();

    let sqlRes = this.setWhereQuery(params, sql);
    if (!sqlRes.values) {
      return await query.query(sqlRes.sql);
    }

    return await query.query(sqlRes.sql, sqlRes.values);
  }

  findOne = async (params) => {
    logger.info('[auth.model findOne] start.');
    let sql = this.selectQuery();

    let sqlRes = this.setWhereQuery(params, sql);
    if (!sqlRes.values) {
      return await query.query(sqlRes.sql);
    }

    
    const result = await query.query(sqlRes.sql, sqlRes.values);

    return result[0];
  }

  create = async (params) => {
    logger.info('[auth.models create] start.');

    const {columnSet, values} = multipleColumnSet(params);
    let sql = `INSERT INTO ${this.tableName} SET ${columnSet}`;

    const result = await query.query(sql, [...values]);
    if (!result) {
      logger.info('[auth.models create] create user error');
      throw new HttpException(500, 'Something went wrong');
    }

    // console.log(result)
    // const affectedRows = result ? result.affectedRows : 0;
    return result;
  }

  updateLoginDate = async (params, id) => {
    logger.info('[auth.models update] start.');

    const {columnSet, values} = multipleColumnSet(params);
    let sql = `UPDATE ${this.tableName} SET ${columnSet} WHERE id = ?`;

    const result = await query.query(sql, [...values, id]);
    if (!result) {
      logger.info('[auth.models update] update user error');
      throw new HttpException(500, 'Something went wrong');
    }

    return result;
  }

  selectQuery() {
    let sql = `SELECT ${this.tableName}.* FROM ${this.tableName} `;
    return sql;
  }

  setWhereQuery(params = {}, sql) {
    let result = {};
    if (!Object.keys(params).length) {
      result.sql = `${sql} WHERE ORDER BY full_name`;
      return result;
    }

    const {columnSet, values} = multipleColumnSetWhere(params);
    result.sql = `${sql} WHERE ${columnSet} ORDER BY full_name`;
    result.values = [...values];

    return result;
  }

  async toColumnSetAsync (params) {
    return await multipleColumnSet(params);
  }
}

module.exports = new UserModel;
